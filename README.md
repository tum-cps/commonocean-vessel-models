# Vessel Models of CommonOcean

This repository contains all vessel models of the [CommonOcean benchmarks](https://commonocean.cps.cit.tum.de/).

We provide implementation examples in Python, routines to convert initial states and parameters, simulation examples to demonstrate the advantages of more complicated models, and a detailed documentation.

## Installation (Python)

To use vehicle models and parameters, add the folder `Python` to your PYTHONPATH. If you are using PyCharm, this can be set through File->Settings->Project Interpreter->Show all->Edit

An alternative would be installing as a module by doing the following commands in the `Python` folder.
```
python setup.py build
python setup.py install
```

## Contribute

If you want to contribute new vehicle models, you can create a merge request or contact us via [mail](mailto:commonocean@lists.lrz.de).

# Contibutors and Reference

We thank all the contibutors for helping develop this project (see contibutors.txt).

**If you use our converter for research, please consider citing our paper:**
```
@inproceedings{Krasowski2022a,
	author = {Krasowski, Hanna and Althoff, Matthias},
	title = {CommonOcean: Composable Benchmarks for Motion Planning on Oceans},
	booktitle = {Proc. of the IEEE International Conference on Intelligent Transportation Systems},
	year = {2022},
}
```
