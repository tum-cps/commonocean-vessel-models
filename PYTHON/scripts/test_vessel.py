"""
Functions for testing different vessel models
"""
import numpy as np
from scipy.integrate import odeint
import numpy
import matplotlib.pyplot as plt
from matplotlib.pyplot import title, legend
import math

from vesselmodels.parameters_vessel_1 import parameters_vessel_1
from vesselmodels.parameters_vessel_2 import parameters_vessel_2
from vesselmodels.parameters_vessel_3 import parameters_vessel_3
from vesselmodels.vessel_dynamics_vp import vessel_dynamics_vp
from vesselmodels.vessel_dynamics_pm import vessel_dynamics_pm
from vesselmodels.vessel_dynamics_yp import vessel_dynamics_yp
from vesselmodels.vessel_dynamics_3F import vessel_dynamics_3f
from vesselmodels.init_pm import init_pm
from vesselmodels.init_3f import init_3f


def func_PM(x, t, u, p):
    f = vessel_dynamics_pm(x, u, p)
    return f


def func_VP(x, t, u, p, dt):
    f = vessel_dynamics_vp(x, u, p, dt)
    return f


def func_YP(x, t, u, p):
    f = vessel_dynamics_yp(x, u, p)
    return f

def func_3F(x, t, u, p):
    f = vessel_dynamics_3f(x, u, p)
    return f



# load parameters
p = parameters_vessel_1()
p = parameters_vessel_2()
p= parameters_vessel_3()

# set options --------------------------------------------------------------
tStart = 0  # start time
tFinal = 120  # end time

px = 0
py = 0
Psi = 0
dotPsi = 0
veln = 10.0

x0_PM = init_pm([px, py, veln, Psi]) # initial state for point mass model
x0_VP = init_pm([px, py, veln, Psi]) # initial state for velocity-constrained point mass model
x0_YP = [px, py, Psi, veln]  # initial state for yaw-constrained point mass model
x0_3F = init_3f([px, py, Psi, veln, dotPsi]) # initial state for 3F model
# --------------------------------------------------------------------------

def turning_left():
    t = numpy.arange(0, tFinal, 0.1)
    dt = 0.1
    u_PM = np.array([0.0, p.a_max])
    u_VP = np.array([0.0, p.a_max])
    u_YP = np.array([0.0, p.w_max])
    u_3F = np.array([0.0, 0.0, p.yaw_moment_max])

    # simulate
    x_brake_PM = odeint(func_PM, x0_PM, t, args=(u_PM, p))
    x_brake_VP = odeint(func_VP, x0_VP, t, args=(u_VP, p, dt))
    x_brake_YP = odeint(func_YP, x0_YP, t, args=(u_YP, p))
    x_brake_3F = odeint(func_3F, x0_3F, t, args=(u_3F, p))

    # position
    plt.plot([tmp[0] for tmp in x_brake_PM], [tmp[1] for tmp in x_brake_PM])
    plt.plot([tmp[0] for tmp in x_brake_VP], [tmp[1] for tmp in x_brake_VP])
    plt.plot([tmp[0] for tmp in x_brake_YP], [tmp[1] for tmp in x_brake_YP])
    plt.plot([tmp[0] for tmp in x_brake_3F], [tmp[1] for tmp in x_brake_3F])
    #plt.title('position')
    legend(['PM', 'VP', 'YP', '3F'])
    plt.xlabel('y-position [m]')
    plt.ylabel('x-position [m]')
    #plt.savefig('results/position_turn_left',format='svg')
    plt.show()

    # velocity
    plt.plot(t, [np.sqrt(tmp[3]**2 + tmp[2]**2) for tmp in x_brake_PM])
    plt.plot(t, [np.sqrt(tmp[3]**2 + tmp[2]**2) for tmp in x_brake_VP])
    plt.plot(t, [np.abs(tmp[3]) for tmp in x_brake_YP])
    plt.plot(t, [np.sqrt(tmp[3]**2 + tmp[4]**2) for tmp in x_brake_3F])
    #plt.title('absolute velocity')
    plt.xlabel('time [s]')
    plt.ylabel('absolute velocity [m/s]')
    legend(['PM', 'VP', 'YP', '3F'])
    #plt.savefig('results/velocity_turn_left', format='svg')
    plt.show()

    plt.plot(t, [tmp[2] for tmp in x_brake_PM])
    plt.plot(t, [tmp[2] for tmp in x_brake_VP])
    plt.plot(t, [tmp[3] * math.cos(tmp[2]) for tmp in x_brake_YP])
    plt.title('x velocity')
    legend(['PM', 'VP', 'YP'])
    plt.show()

    plt.plot(t, [tmp[3] for tmp in x_brake_PM])
    plt.plot(t, [tmp[3] for tmp in x_brake_VP])
    plt.plot(t, [tmp[3] * math.sin(tmp[2]) for tmp in x_brake_YP])
    plt.title('y velocity')
    legend(['PM', 'VP', 'YP'])
    plt.show()

def turning_right():
    t = numpy.arange(0, tFinal, 0.1)
    dt = 0.1
    u_PM = np.array([0.0, - p.a_max])
    u_VP = np.array([0.0, - p.a_max])
    u_YP = np.array([0.0, -p.w_max])
    u_3F = np.array([0.0, 0.0, -p.yaw_moment_max])

    # simulate
    x_brake_PM = odeint(func_PM, x0_PM, t, args=(u_PM, p))
    x_brake_VP = odeint(func_VP, x0_VP, t, args=(u_VP, p, dt))
    x_brake_YP = odeint(func_YP, x0_YP, t, args=(u_YP, p))
    x_brake_3F = odeint(func_3F, x0_3F, t, args=(u_3F, p))

    # position
    plt.plot([tmp[0] for tmp in x_brake_PM], [tmp[1] for tmp in x_brake_PM])
    plt.plot([tmp[0] for tmp in x_brake_VP], [tmp[1] for tmp in x_brake_VP])
    plt.plot([tmp[0] for tmp in x_brake_YP], [tmp[1] for tmp in x_brake_YP])
    plt.plot([tmp[0] for tmp in x_brake_3F], [tmp[1] for tmp in x_brake_3F])
    plt.title('position')
    legend(['PM', 'VP', 'YP', '3F'])
    plt.show()

    # velocity
    plt.plot(t, [np.sqrt(tmp[3]**2 + tmp[2]**2) for tmp in x_brake_PM])
    plt.plot(t, [np.sqrt(tmp[3]**2 + tmp[2]**2) for tmp in x_brake_VP])
    plt.plot(t, [np.abs(tmp[3]) for tmp in x_brake_YP])
    plt.plot(t, [np.sqrt(tmp[3]**2 + tmp[4]**2) for tmp in x_brake_3F])
    plt.title('absolute velocity')
    legend(['PM', 'VP', 'YP', '3F'])
    plt.show()

    plt.plot(t, [tmp[2] for tmp in x_brake_PM])
    plt.plot(t, [tmp[2] for tmp in x_brake_VP])
    plt.plot(t, [tmp[3] * math.cos(tmp[2]) for tmp in x_brake_YP])
    plt.title('x velocity')
    legend(['PM', 'VP', 'YP'])
    plt.show()

    plt.plot(t, [tmp[3] for tmp in x_brake_PM])
    plt.plot(t, [tmp[3] for tmp in x_brake_VP])
    plt.plot(t, [tmp[3] * math.sin(tmp[2]) for tmp in x_brake_YP])
    plt.title('y velocity')
    legend(['PM', 'VP', 'YP'])
    plt.show()

def braking():
    t = numpy.arange(0, tFinal, 0.1)
    dt = 0.1
    u_PM = np.array([- p.a_max, 0.0])
    u_VP = np.array([- p.a_max, 0.0])
    u_YP = np.array([- p.a_max, 0.0])
    u_3F =np.array( [-p.surge_force_max, 0.0, 0.0])

    # simulate
    x_brake_PM = odeint(func_PM, x0_PM, t, args=(u_PM, p))
    x_brake_VP = odeint(func_VP, x0_VP, t, args=(u_VP, p, dt))
    x_brake_YP = odeint(func_YP, x0_YP, t, args=(u_YP, p))
    x_brake_3F = odeint(func_3F, x0_3F, t, args=(u_3F, p))

    # position
    plt.plot([tmp[0] for tmp in x_brake_PM], [tmp[1] for tmp in x_brake_PM])
    plt.plot([tmp[0] for tmp in x_brake_VP], [tmp[1] for tmp in x_brake_VP])
    plt.plot([tmp[0] for tmp in x_brake_YP], [tmp[1] for tmp in x_brake_YP])
    plt.plot([tmp[0] for tmp in x_brake_3F], [tmp[1] for tmp in x_brake_3F])
    plt.title('position')
    legend(['PM', 'VP', 'YP', '3F'])
    plt.show()

    # velocity
    plt.plot(t, [np.sqrt(tmp[3]**2 + tmp[2]**2) for tmp in x_brake_PM])
    plt.plot(t, [np.sqrt(tmp[3]**2 + tmp[2]**2) for tmp in x_brake_VP])
    plt.plot(t, [np.abs(tmp[3]) for tmp in x_brake_YP])
    plt.plot(t, [np.sqrt(tmp[3]**2 + tmp[4]**2) for tmp in x_brake_3F])
    plt.title('absolute velocity')
    legend(['PM', 'VP', 'YP', '3F'])
    plt.show()

    plt.plot(t, [tmp[2] for tmp in x_brake_PM])
    plt.plot(t, [tmp[2] for tmp in x_brake_VP])
    plt.plot(t, [tmp[3]*math.cos(tmp[2]) for tmp in x_brake_YP])
    plt.title('x velocity')
    legend(['PM', 'VP', 'YP'])
    plt.show()

    plt.plot(t, [tmp[3] for tmp in x_brake_PM])
    plt.plot(t, [tmp[3] for tmp in x_brake_VP])
    plt.plot(t, [tmp[3]*math.sin(tmp[2]) for tmp in x_brake_YP])
    plt.title('y velocity')
    legend(['PM', 'VP', 'YP'])
    plt.show()


def accelerating():
    t = numpy.arange(0, tFinal, 0.1)
    dt = 0.1
    u_PM = np.array([ p.a_max,  0.0])
    u_VP = np.array([ p.a_max,  0.0])
    u_YP = np.array([ p.a_max, 0.0])
    u_3F = np.array([p.surge_force_max, 0.0, 0.0])

    # simulate
    x_brake_PM = odeint(func_PM, x0_PM, t, args=(u_PM, p))
    x_brake_VP = odeint(func_VP, x0_VP, t, args=(u_VP, p, dt))
    x_brake_YP = odeint(func_YP, x0_YP, t, args=(u_YP, p))
    x_brake_3F = odeint(func_3F, x0_3F, t, args=(u_3F, p))

    # position
    plt.plot([tmp[0] for tmp in x_brake_PM], [tmp[1] for tmp in x_brake_PM])
    plt.plot([tmp[0] for tmp in x_brake_VP], [tmp[1] for tmp in x_brake_VP])
    plt.plot([tmp[0] for tmp in x_brake_YP], [tmp[1] for tmp in x_brake_YP])
    plt.plot([tmp[0] for tmp in x_brake_3F], [tmp[1] for tmp in x_brake_3F])
    plt.title('position')
    legend(['PM', 'VP', 'YP', '3F'])
    plt.show()

    plt.plot(t, [tmp[0] for tmp in x_brake_PM])
    plt.plot(t, [tmp[0] for tmp in x_brake_VP])
    plt.plot(t, [tmp[0] for tmp in x_brake_YP])
    plt.plot(t, [tmp[0] for tmp in x_brake_3F])
    legend(['PM', 'VP', 'YP', '3F'])
    plt.xlabel('time [s]')
    plt.ylabel('x-position [m]')
    #plt.savefig('results/position_acc',format='svg')
    plt.show()

    # velocity
    plt.plot(t, [np.sqrt(tmp[3]**2 + tmp[2]**2) for tmp in x_brake_PM])
    plt.plot(t, [np.sqrt(tmp[3]**2 + tmp[2]**2) for tmp in x_brake_VP])
    plt.plot(t, [np.abs(tmp[3]) for tmp in x_brake_YP])
    plt.plot(t, [np.sqrt(tmp[3]**2 + tmp[4]**2) for tmp in x_brake_3F])
    #plt.title('absolute velocity')
    plt.xlabel('time [s]')
    plt.ylabel('absolute velocity [m/s]')
    legend(['PM', 'VP', 'YP', '3F'])
    #plt.savefig('results/velocity_acc', format='svg')
    plt.show()

    plt.plot(t, [tmp[2] for tmp in x_brake_PM])
    plt.plot(t, [tmp[2] for tmp in x_brake_VP])
    plt.plot(t, [tmp[3] * math.cos(tmp[2]) for tmp in x_brake_YP])
    plt.title('x velocity')
    legend(['PM', 'VP', 'YP'])
    plt.show()

    plt.plot(t, [tmp[3] for tmp in x_brake_PM])
    plt.plot(t, [tmp[3] for tmp in x_brake_VP])
    plt.plot(t, [tmp[3] * math.sin(tmp[2]) for tmp in x_brake_YP])
    plt.title('y velocity')
    legend(['PM', 'VP', 'YP'])
    plt.show()


# run simulations *****************
if __name__ == '__main__':
    #braking()
    accelerating()
    turning_left()
    #turning_right()
