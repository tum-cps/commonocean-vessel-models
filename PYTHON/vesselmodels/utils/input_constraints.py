import numpy as np

__author__ = "Hanna Krasowski"
__copyright__ = "TUM Cyber-Physical Systems Group"
__version__ = "2022a"
__maintainer__ = "Hanna Krasowski"
__email__ = "commonocean@lists.lrz.de"
__status__ = "Released"

def yaw_constraints(yaw, p):
    y = yaw
    # yaw limit reached?
    if y <= -p.w_max:
        yaw = - p.w_max
    elif y >= p.w_max:
        yaw = p.w_max

    return yaw

def acceleration_constraints(acceleration, p):
    
    a = np.linalg.norm(acceleration)

    # acceleration limit reached?
    if a >= p.a_max:
        acceleration = (acceleration/a) * p.a_max

    return acceleration

def velocity_constraints(velocity_prev, acceleration, p, dt, vehicle_model=0, orientation = None):

    if vehicle_model == 0 or vehicle_model == 1:
        # Velocity at next time step
        velocity = velocity_prev + (acceleration*dt)
        v = np.linalg.norm(velocity)

        if v + 10e-8 >= p.v_max and np.linalg.norm(velocity_prev) <= p.v_max:
            velocity_new = (velocity/v) * p.v_max
            # correction of acceleration inputs
            acc_limits = (velocity_new - velocity_prev) / dt
            for idx, limit in enumerate(acc_limits):
                if acc_limits[idx] < 0.0:
                    acceleration[idx] = np.clip(acceleration[idx], limit, p.a_max)
                else:
                    acceleration[idx] = np.clip(acceleration[idx], -p.a_max, limit)
            return acceleration
        elif np.linalg.norm(velocity_prev) > p.v_max:
            for idx, vel in enumerate(velocity_prev):
                if (np.sign(vel) == -1 and np.sign(acceleration[idx]) == -1) or (np.sign(vel) == 1 and np.sign(acceleration[idx]) == 1):
                    acceleration[idx] = 0.0
            return acceleration
        else:
            return acceleration
    elif vehicle_model == 2:

        if orientation is None:
            raise ValueError("For the YP model, the orientation needs to be provided!")
        else:
            pass

        if (velocity_prev + 10e-8 >= p.v_max and acceleration > 0) or (velocity_prev - 10e-8 <= -p.v_max and acceleration < 0):
            return 0.0

        velocity = velocity_prev + (acceleration*dt)
        v = np.linalg.norm(velocity)

        # note: current assumption is that it is not possible by the action limits to go from one velocity boundary to the other in one time step
        if v >= p.v_max and ((velocity < 0 and acceleration < 0) or (velocity > 0 and acceleration > 0)):
            velocity_new = np.sign(velocity) * p.v_max
            # correction of acceleration inputs
            acc_limit = (velocity_new - velocity_prev) / dt
            if velocity_new < 0.0:
                acceleration = np.clip(acceleration, acc_limit, p.a_max)
            else:
                acceleration = np.clip(acceleration, -p.a_max, acc_limit)

        return acceleration

    else:
        raise ValueError("Due to computational reasons, use simplified Euler correction manually instead!")


def general_bounded_constraint(u,limit):

    if u <= - limit:
        u = - limit
    elif u >= limit:
        u = limit

    return u


